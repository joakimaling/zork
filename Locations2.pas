{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 14, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit Locations;

{$mode objfpc}

interface
{--------------------------------------------------------------------------------------------------}
uses
	Items, Portals, Shared;
{--------------------------------------------------------------------------------------------------}
type
	TLocation = class
	private
		Title, Description: string; //A short and a long description of the room or area...
		Paths: array[TDirections] of record
				   Location: TLocation;
				   Portal: TPortal;
			   end;
		Items: array of TItem;
		Placement: TPlacements; //For deciding whether the player can go in/out...
		Visited, Dark: boolean; //If the location is visited then the area decription won't appear...
		function GetPath(Direction: TDirections): TLocation;
		function GetPort(Direction: TDirections): TPortal;
		function FindPort(Port: string): TPortal;
		function FindItem(Item: string): TItem;
	public
		constructor Create(Name, Information: string; Place: TPlacements; IsDark: boolean);
		procedure AddPath(Direction: TDirections; Path: TLocation); overload;
		procedure AddPath(Direction: TDirections; Path: TLocation; Portal: TPortalTypes; UnlockedBy: TUnlockingTypes); overload;
		procedure AddPath(Direction: TDirections; Path: TLocation; Portal: TPortalTypes; State: TPortalStatus); overload;
		procedure AddPort(Direction: TDirections; Port: TPortal);
		procedure AddItem(Item: TItem);
		function GetItem(Item: string): TItem;
		procedure Brief;
		procedure Look;
		function Find(Something: string): pointer;
		property Path[Direction: TDirections]: TLocation read GetPath;
		property Port[Direction: TDirections]: TPortal read GetPort;
	end;
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
constructor TLocation.Create(Name, Information: string; Place: TPlacements; IsDark: boolean);
var
	Index: byte;
begin
	for Index := byte(Down) to byte(SouthEast) do begin
		Paths[TDirections(Index)].Location := Blocked;
		Paths[TDirections(Index)].Portal := None;
	end;
	Title := Name;
	Description := Information;
	Items := None;
	Placement := Place;
	Visited := false;
	Dark := IsDark;
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.AddPath(Direction: TDirections; Path: TLocation); overload;
begin
	if Paths[Direction].Location = Blocked then begin //Stops recursion, limits path to be set once
		Paths[Direction].Location := Path;
		if Direction in [Down, East, North, NorthEast, NorthWest] then
			Path.AddPath(TDirections(byte(Direction) + 5), self)
		else
			Path.AddPath(TDirections(byte(Direction) + 5), self);
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.AddPath(Direction: TDirections; Path: TLocation; Portal: TPortalTypes; UnlockedBy: TUnlockingTypes); overload;
var
	NewPort: TPortal;
begin
	NewPort := TPortal.Create(Portal, UnlockedBy);
	Paths[Direction].Location := Path;
	Paths[Direction].Portal := NewPort;
	if Direction in [Down, East, North, NorthEast, NorthWest] then begin
		Path.AddPath(TDirections(byte(Direction) + 5), self);
		Path.AddPort(TDirections(byte(Direction) + 5), NewPort);
	end else begin
		Path.AddPath(TDirections(byte(Direction) - 5), self);
		Path.AddPort(TDirections(byte(Direction) - 5), NewPort);
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.AddPath(Direction: TDirections; Path: TLocation; Portal: TPortalTypes; State: TPortalStatus); overload;
var
	NewPort: TPortal;
begin
	NewPort := TPortal.Create(Portal, State);
	Paths[Direction].Location := Path;
	Paths[Direction].Portal := NewPort;
	if Direction in [Down, East, North, NorthEast, NorthWest] then begin
		Path.AddPath(TDirections(byte(Direction) + 5), self);
		Path.AddPort(TDirections(byte(Direction) + 5), NewPort);
	end else begin
		Path.AddPath(TDirections(byte(Direction) - 5), self);
		Path.AddPort(TDirections(byte(Direction) - 5), NewPort);
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.AddPort(Direction: TDirections; Port: TPortal);
begin
	if Paths[Direction].Portal = None then Paths[Direction].Portal := Port;
end;
{--------------------------------------------------------------------------------------------------}
function TLocation.GetPath(Direction: TDirections): TLocation;
begin
	if (Paths[Direction].Portal <> None) and (Paths[Direction].Portal.State <> Opened) then
		GetPath := Blocked
	else
		GetPath := Paths[Direction].Location;
end;
{--------------------------------------------------------------------------------------------------}
function TLocation.GetPort(Direction: TDirections): TPortal;
begin
	GetPort := Paths[Direction].Portal;
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.AddItem(Item: TItem);
begin
	if Items = None then SetLength(Items, 1) else SetLength(Items, Length(Items) + 1);
	Items[Length(Items) - 1] := Item;
end;
{--------------------------------------------------------------------------------------------------}
function TLocation.GetItem(Item: string): TItem;
var
	Index: byte;
begin
	GetItem := None;
	for Index := 0 to Length(Items) - 1 do
		if (Items[Index] <> None) and (Items[Index].Name = Item) then begin
			GetItem := Items[Index];
			Items[Index] := None;
			exit;
		end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.Brief;
begin
	if Dark then
		WriteLn('The room is pitch dark. You cannot see anything')
	else
		if not Visited then begin
			Visited := true;
			WriteLn(Title);
			WriteLn(Description);
		end else
			WriteLn(Title);
end;
{--------------------------------------------------------------------------------------------------}
procedure TLocation.Look;
var
	Index: byte;
begin
	if not Dark then begin
		WriteLn(Title);
		WriteLn(Description);
		//todo: Description of the path-ways from this location...
		if Items <> None then
			for Index := 0 to Length(Items) - 1 do
				if Items[Index] <> None then
					WriteLn('There is a ', Items[Index].Name, ' here');
	end else
		WriteLn('The room is pitch dark. You cannot see anything');
end;
{--------------------------------------------------------------------------------------------------}
function TLocation.FindPort(Port: string): TPortal;
var
	Index: TDirections;
begin
	for Index := Down to SouthEast do
		if (Paths[Index].Portal <> None) and (Paths[Index].Portal.Name = Port) then break;
	FindPort := Paths[Index].Portal;
end;
{--------------------------------------------------------------------------------------------------}
function TLocation.FindItem(Item: string): TItem;
var
	Index: byte;
begin
	FindItem := None;
	for Index := 0 to Length(Items) - 1 do
		if Items[Index] <> None then FindItem := Items[Index];
end;
{--------------------------------------------------------------------------------------------------}
function TLocation.Find(Something: string): pointer;
var
	Item: TItem;
begin
	Item := FindItem(Something);
	if Item = None then
		Find := FindPort(Something)
	else
		Find := Item;
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.