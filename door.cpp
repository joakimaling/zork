#include "door.h"
//----------------------------------------------------------------------------
Door::Door() {
	state = CLOSED;
//	this.key = key;
}
//----------------------------------------------------------------------------
Door::~Door() {

}
//----------------------------------------------------------------------------
//bool Door::unlock(Key *key) {
//	if(state == LOCKED && this.key == key) {
//		state = CLOSED;
//		return true;
//	}
//	return false;
//}
//----------------------------------------------------------------------------
bool Door::close() {
	if(state == OPEN) {
		state = CLOSED;
		return true;
	}
	return false;
}
//----------------------------------------------------------------------------
bool Door::open() {
	if(state == CLOSED) {
		state = OPEN;
		return true;
	}
	return false;
}
//----------------------------------------------------------------------------
DoorState Door::examine() {
	return state;
}
//----------------------------------------------------------------------------
bool Door::is_synonym(std::string word) {
	for(int i = 0; i < (int)synonyms.size(); i++) {
		if(synonyms[i] == word) {
			return true;
		}
	}
	return false;
}
