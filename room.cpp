#include "room.h"
#include <cstdlib>
//----------------------------------------------------------------------------
Room::Room() {
//	for(int i = 0; i < MAX_ITEMS; i++) {
//		item[i] = NULL;
//	}
	visited = false;
}
//----------------------------------------------------------------------------
Room::~Room() {
//	for(int i = 0; i < MAX_ITEMS; i++) {
//		if(item[i]) {
//			delete item[i];
//		}
//	}
}
//----------------------------------------------------------------------------
void Room::draw(bool force, bool player_lit) {
	if(!player_lit) {
		std::cout << name << std::endl;
		if(force || !visited) {
			std::cout << description << std::endl;
			visited = true;
		}
		for(int i = 0; i < (int)items.size(); i++) {
			if(items[i]->visible) {
				std::cout << "There is a " << items[i]->name << " here." << std::endl;
				items[i]->look();
			}
		}
	}
	else {
		if(!visited) {
			std::cout << "You have moved into a dark place" << std::endl;
		}
		std::cout << "It is pitch black. You are likely to be eaten by a grue." << std::endl;
	}
}
//----------------------------------------------------------------------------
std::vector<Room*> Room::get_adjacent_rooms(Direction direction) {
	switch(direction) {
		case OUT:
			return get_adjacent_outdoor_rooms();
		case IN:
			return get_adjacent_indoor_rooms();
	}
}
//----------------------------------------------------------------------------
std::vector<Room*> Room::get_adjacent_outdoor_rooms() {
	std::vector<Room*> adjacent_rooms;
	for(int i = 0; i < DIR_SIZE - 2; i++) {
		if(path[i].room && !path[i].room->inside) {
			 adjacent_rooms.push_back(path[i].room);
		}
	}
	return adjacent_rooms;
}
//----------------------------------------------------------------------------
std::vector<Room*> Room::get_adjacent_indoor_rooms() {
	std::vector<Room*> adjacent_rooms;
	for(int i = 0; i < DIR_SIZE - 2; i++) {
		if(path[i].room && path[i].room->inside) {
			 adjacent_rooms.push_back(path[i].room);
		}
	}
	return adjacent_rooms;
}
//----------------------------------------------------------------------------
Room* Room::get_indoor_room() {
	for(int i = 0; i < DIR_SIZE - 2; i++) {
		if(path[i].room && path[i].room->inside) {
			return path[i].room;
		}
	}
	return NULL;
}
//----------------------------------------------------------------------------
Room* Room::get_outdoor_room() {
	for(int i = 0; i < DIR_SIZE - 2; i++) {
		if(path[i].room && !path[i].room->inside) {
			return path[i].room;
		}
	}
	return NULL;
}
//----------------------------------------------------------------------------
Door* Room::get_first_door() {
	for(int i = 0; i < DIR_SIZE - 2; i++) {
		if(path[i].door) {
			return path[i].door;
		}
	}
	return NULL;
}
//----------------------------------------------------------------------------
Door* Room::get_door(std::string word) {
	for(int i = 0; i < DIR_SIZE - 2; i++) {
		if(path[i].door && path[i].door->is_synonym(word)) {
			return path[i].door;
		}
	}
	return NULL;
}
