{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 13, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit Parser;

interface
{--------------------------------------------------------------------------------------------------}
uses
	Adventurer, Items, Shared, SysUtils;
{--------------------------------------------------------------------------------------------------}
type
	PString = array of string;
{--------------------------------------------------------------------------------------------------}
procedure Parse(Player: TPlayer; Command: string);
{}
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
function SplitPhrase(KeyWord, Sentence: string): PString;
var
	Part: PString;
	Index: byte = 0;
	SplitAt: byte;
begin
	repeat
		SplitAt := Pos(KeyWord, Sentence);
		if SplitAt <> 0 then begin
			SetLength(Part, Index + 1);
			Part[Index] := Copy(Sentence, 1, SplitAt);
			Sentence := Copy(Sentence, SplitAt + Length(KeyWord) + 1, Length(Sentence));
			Inc(Index);
		end;
	until SplitAt = 0;
	Part[Index] := Sentence;
	SplitPhrase := Part;
end;
{--------------------------------------------------------------------------------------------------}
procedure ParseVerbGrammar(ImperativeVerb, GrammarLine: string);
var
	Preposition, NounPhrase: string;
	SplitAt: byte;
begin
	SplitAt := Pos(' ', GrammarLine);
	if SplitAt <> 0 then begin
		Preposition := Copy(GrammarLine, 1, SplitAt);
		NounPhrase := Copy(GrammarLine, SplitAt + 2, Length(GrammarLine));

	end;


end;
{--------------------------------------------------------------------------------------------------}
procedure ParseSingleWord(SingleWord: string);
begin

end;
{--------------------------------------------------------------------------------------------------}
procedure Parse(Player: TPlayer; Command: string);
const
	LastCommand: string = ''; //For storing the last single command
var
	ActionPhrase, ImperativeVerb, GrammarLine, NounPhrase: string;
	VerbPhrase: Pstring;
	SplitAt, Index: byte;
begin
	Command := LowerCase(Command); //To simplify testing...
{1}	SplitAt := Pos(', ', Command);
	if SplitAt <> 0 then begin
		NounPhrase := Copy(Command, 1, SplitAt);
		ActionPhrase := Copy(Command, SplitAt + 2, Length(Command));
	end else begin
		ActionPhrase := Command;
	end;
{2}	VerbPhrase := SplitPhrase(' then ', ActionPhrase);
	for Index := 0 to Length(VerbPhrase) - 1 do begin
{3}		if VerbPhrase[Index] = 'again' then VerbPhrase[Index] := LastCommand;
		SplitAt := Pos(' ', VerbPhrase[Index]);
		if SplitAt <> 0 then begin
			ImperativeVerb := Copy(VerbPhrase[Index], 1, SplitAt);
			GrammarLine := Copy(VerbPhrase[Index], SplitAt + 2, Length(VerbPhrase[Index]));
			ParseVerbGrammar(ImperativeVerb, GrammarLine);
		end else
			ParseSingleWord(VerbPhrase[Index]);
	end;
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.



{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... May 17, 2012                                                                         }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
program Parser;

{$mode objfpc}
{$M+}

// uses
// 	Classes;
{--------------------------------------------------------------------------------------------------}
const
	Verbs = 'answer attack blow break burn climb close count cross cut dig drink drop eat enter examine exit fill follow get go jump kick knock kill light listen lock look lower move open pour pray pull push put raise read say search slide smell stay strike swim take tell throw tie touch turn unlock wake walk wave wear wind';
	Nouns = 'book leaflet mailbox ';


{--------------------------------------------------------------------------------------------------}
type
	TString = array of String;
{--------------------------------------------------------------------------------------------------}
var
	TypedCommand: String;
{--------------------------------------------------------------------------------------------------}
function Split(Delimiter, DelimtedString: String): TString;
var
	SplitAt: Byte;
begin
	repeat
		SplitAt := Pos(Delimiter, DelimtedString);
		SetLength(Split, Length(Split) + 1);
		if SplitAt > 0 then begin
			Split[High(Split)] := Copy(DelimtedString, 1, SplitAt);
			DelimtedString := Copy(DelimtedString, SplitAt + Length(Delimiter), Length(DelimtedString));
		end;
	until SplitAt = 0;
	Split[High(Split)] := DelimtedString;
end;
{--------------------------------------------------------------------------------------------------}
procedure ParserError(Error: String);
begin
	WriteLn(Error);
end;
{--------------------------------------------------------------------------------------------------}
procedure SingleWord(Command: String);
begin
	if Pos(Command, Verbs) > 0 then
		WriteLn('You ', Command)
	else
		ParserError('S I don''t know...');
end;
{--------------------------------------------------------------------------------------------------}
procedure DoubleWord(Command: String);
var
	Part: array[0..1] of String;
begin
	Part := Split(#32, Command)
	if Pos(Part[0], Verbs) > 0 then
		WriteLn('You ', )
	else
		ParserError('D I don''t know...');
end;
{--------------------------------------------------------------------------------------------------}
procedure ParseCommand(Command: String);
var
	Splitted: TString;
	Index: Byte;
begin
	Splitted := Split(' then ', LowerCase(Command));
	for Index := 0 to High(Splitted) do begin
		if Pos(#32, Splitted[Index]) = 0 then
			SingleWord(Splitted[Index])
		else
			DoubleWord(Splitted[Index]);
	end;
end;
{--------------------------------------------------------------------------------------------------}
begin
	Write('> ');
	ReadLn(TypedCommand);
	WriteLn;
	ParseCommand(TypedCommand);
end.



{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 13, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description The parser for interactive games where commands are given by typing text into the    }
{             console window. The parser understands sentences described in the parser.odt file.   }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit Parser;

interface
{--------------------------------------------------------------------------------------------------}
uses
	Adventurer, Items, Shared;
{--------------------------------------------------------------------------------------------------}
const
	PrepositionList: array[0..9] of string = ('at', 'beneath', 'beside', 'in', 'inside', 'into', 'on', 'to', 'under', 'underneath');
	ThenWord: array[0..1] of string = ('. ', ' then ');
{--------------------------------------------------------------------------------------------------}
type
	PString = array of string;
{--------------------------------------------------------------------------------------------------}
procedure Parse(Player: TPlayer; Command: string);
{}
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
var
	LastCommand: string = ''; //This variable is supposed to allow repetition of a previous command
{--------------------------------------------------------------------------------------------------}
procedure ParseVerbNoun(Player: TPlayer; Verb, Noun: string);
var
	Ptr: pointer;
begin
	if Verb = 'drop' then Player.Drop(Noun);

	//if Verb = 'eat' then Player.Eat(Noun);

	if (Verb = 'get') or (Verb = 'take') then Player.Get(Noun);

	if (Verb = 'go') and (Noun = 'back') then Player.Go(Back);
	if (Verb = 'go') and (Noun = 'east') then Player.Go(East);
	if (Verb = 'go') and (Noun = 'north') then Player.Go(North);
	if (Verb = 'go') and (Noun = 'south') then Player.Go(South);
	if (Verb = 'go') and (Noun = 'west') then Player.Go(West);
	if ((Verb = 'go') or (Verb = 'climb')) and (Noun = 'up') then Player.Go(Up);
	if ((Verb = 'go') or (Verb = 'climb')) and (Noun = 'down') then Player.Go(Down);

	if Verb = 'open' then begin
		Ptr := Player.Location.Find(Noun);
		Ptr.Open;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure ParseSingleWord(Player: TPlayer; SingleWord: string);
begin
	//if SingleWord = 'again' then Parse(Player, LastCommand);
	if SingleWord = 'diagnose' then Player.Diagnose;
	if SingleWord = 'look' then Player.Location.Look;
	if SingleWord = 'view' then Player.View;
	if SingleWord = 'quit' then Halt;
	if SingleWord = '' then WriteLn('I beg your pardon?');
end;
{--------------------------------------------------------------------------------------------------}
function SplitPhrase(KeyWord, Sentence: string): PString; overload;
var
	Index: byte = 0;
	SplitAt: byte;
begin
	repeat
		SplitAt := Pos(KeyWord, Sentence);
		SetLength(SplitPhrase, Index + 1);
		if SplitAt <> 0 then begin
			SplitPhrase[Index] := Copy(Sentence, 1, SplitAt - 1);
			Sentence := Copy(Sentence, SplitAt + Length(KeyWord), Length(Sentence));
			Inc(Index);
		end;
	until SplitAt = 0;
	SplitPhrase[Index] := Sentence;
end;
{--------------------------------------------------------------------------------------------------}
function SplitPhrase(KeyWord: PString; Sentence: string): PString; overload;
var
	Index: byte = 0;
	Count, Long, Split, SplitAt: byte;
begin
	repeat
		SplitAt := Length(Sentence);
		for Count := 0 to Length(KeyWord) - 1 do begin
			Split := Pos(KeyWord[Count], Sentence);
			if (Split < SplitAt) and (Split <> 0) then begin
				SplitAt := Split;
				Long := Length(KeyWord[Count]);
			end;
		end;
		SetLength(SplitPhrase, Index + 1);
		if (SplitAt <> 0) and (SplitAt <> Length(Sentence)) then begin
			SplitPhrase[Index] := Copy(Sentence, 1, SplitAt - 1);
			Sentence := Copy(Sentence, SplitAt + Long, Length(Sentence));
			Inc(Index);
		end;
	until (SplitAt = 0) or (SplitAt = Length(Sentence));
	SplitPhrase[Index] := Sentence;
end;
{--------------------------------------------------------------------------------------------------}
function IsPreposition(Preposition: string): boolean;
var
	Index: byte;
begin
	IsPreposition := false;
	for Index := 0 to Length(PrepositionList) - 1 do
		if Preposition = PrepositionList[Index] then begin
			IsPreposition := true;
			exit;
		end;
end;
{--------------------------------------------------------------------------------------------------}
procedure ParseVerbGrammar(Player: TPlayer; ImperativeVerb, GrammarLine: string);
var
	Preposition, NounPhrase: string;
	SplitAt, Index: byte;
	BasicNP: PString;
begin
	SplitAt := Pos(' ', GrammarLine);
	if SplitAt <> 0 then begin
		Preposition := Copy(GrammarLine, 1, SplitAt - 1);
{4}		if IsPreposition(Preposition) then
			NounPhrase := Copy(GrammarLine, SplitAt + 1, Length(GrammarLine))
		else
			NounPhrase := GrammarLine;
	end else
		NounPhrase := GrammarLine;
	BasicNP := SplitPhrase(' and ', NounPhrase);
	for Index := 0 to Length(BasicNP) - 1 do ParseVerbNoun(Player, ImperativeVerb, BasicNP[Index]);
end;
{--------------------------------------------------------------------------------------------------}
procedure Parse(Player: TPlayer; Command: string);
var
	ActionPhrase, ImperativeVerb, GrammarLine, NounPhrase: string;
	VerbPhrase: PString;
	SplitAt, Index: byte;
begin
	Command := LowerCase(Command); //To simplify testing...

{1}	SplitAt := Pos(', ', Command);
	if SplitAt <> 0 then begin
		NounPhrase := Copy(Command, 1, SplitAt - 1);
		ActionPhrase := Copy(Command, SplitAt + 2, Length(Command));
	end else begin
		ActionPhrase := Command;
	end;

{2}	VerbPhrase := SplitPhrase(ThenWord, ActionPhrase);

	for Index := 0 to Length(VerbPhrase) - 1 do begin
{3}		//if VerbPhrase[Index] = 'again' then VerbPhrase[Index] := LastCommand;
		SplitAt := Pos(' ', VerbPhrase[Index]);
		if SplitAt <> 0 then begin
			ImperativeVerb := Copy(VerbPhrase[Index], 1, SplitAt - 1);
			GrammarLine := Copy(VerbPhrase[Index], SplitAt + 1, Length(VerbPhrase[Index]));
			ParseVerbGrammar(Player, ImperativeVerb, GrammarLine);
		end else
			ParseSingleWord(Player, VerbPhrase[Index]);
	end;
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.