#include "item.h"
#include <iostream>
//----------------------------------------------------------------------------
Item::Item() {

}
//----------------------------------------------------------------------------
Item::~Item() {

}
//----------------------------------------------------------------------------
void Item::draw() {

}
//----------------------------------------------------------------------------
void Item::list_items() {
	for(int i = 0; i < (int)content.size(); i++) {
		std::cout << ((i > 0) ? ", a " : " a ");
		std::cout << content[i]->name;
	}
	std::cout << "." << std::endl;
}
//----------------------------------------------------------------------------
void Item::look() {
	if(!content.empty() && (transparent || state == OPEN)) {
		std::cout << "The " << name << " contains:" << std::endl;
		for(int i = 0; i < (int)content.size(); i++) {
			std::cout << "  A " << content[i]->name << std::endl;
		}
	}
}
//----------------------------------------------------------------------------
void Item::examine() {
	if(container) {
		if(!content.empty() && (transparent || state == OPEN)) {
			std::cout << "The " << name << " contains:" << std::endl;
			for(int i = 0; i < (int)content.size(); i++) {
				std::cout << "  A " << content[i]->name << std::endl;
			}
		}
		else {
			if(state == CLOSED) {
				std::cout << "The " << name << " is closed" << std::endl;
			}
			else {
				std::cout << "The " << name << " is empty" << std::endl;
			}
		}
	}
	else {
		std::cout << description << std::endl;
	}
}
//----------------------------------------------------------------------------
ContainerResult Item::close() {
	if(container) {
		if(state == OPEN) {
			state = CLOSED;
			return CONT_OK;
		}
		return CONT_ALREADY;
	}
	return CONT_FAIL;
}
//----------------------------------------------------------------------------
ContainerResult Item::open() {
	if(container) {
		if(state == CLOSED) {
			state = OPEN;
			if(!content.empty()) {
				std::cout << "Opening the " << name << " reveals";
				list_items();
				return CONT_OK;
			}
			return CONT_EMPTY;
		}
		return CONT_ALREADY;
	}
	return CONT_FAIL;
}
//----------------------------------------------------------------------------
bool Item::is_synonym(std::string word) {
	for(int i = 0; i < (int)synonyms.size(); i++) {
		if(synonyms[i].compare(word) == 0) {
			return true;
		}
	}
	return name.compare(word) == 0;
}
