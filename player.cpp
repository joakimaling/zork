#include "player.h"
#include <fstream>
//----------------------------------------------------------------------------
Player::Player() {

}
//----------------------------------------------------------------------------
Player::~Player() {

}
//----------------------------------------------------------------------------
void Player::init(const char *file, Map &map) {
	std::ifstream infile(file);
	if(infile.is_open()) {
		infile >> name >> id >> lit >> inventory_size;
		current_room = map.map_grid[--id];
	}
}
//----------------------------------------------------------------------------
GoResult Player::go(Direction direction) {
	if((direction == IN && !current_room->inside) || (direction == OUT && current_room->inside)) {
		tmp = current_room->get_adjacent_rooms(direction);
		switch(tmp.size()) {
			case 1:
				if(!current_room->path[direction].door || current_room->path[direction].door->state == OPEN) {
					previous_room = current_room;
					current_room = tmp[0];
					return GO_OK;
				}
				std::cout << "The " << tmp[0]->name << " is closed." << std::endl;
				return GO_CLOSED;
			case 0:
				return GO_FAIL;
			default:
				return GO_WHERE;
		}
	}
	else if(direction == IN || direction == OUT) {
		return GO_FAIL;
	}
	else if(current_room->path[direction].room) {
		previous_room = current_room;
		current_room = current_room->path[direction].room;
		return GO_OK;
	}
	return GO_FAIL;
}
//----------------------------------------------------------------------------
void Player::show_inventory() {
	if(!inventory.empty()) {
		std::cout << "You are carrying:" << std::endl;
		for(int i = 0; i < (int)inventory.size(); i++) {
			std::cout << "  A " << inventory[i]->name << std::endl;
		}
	}
	else {
		std::cout << "You are empty-handed." << std::endl;
	}
}
//----------------------------------------------------------------------------
GetResult Player::get_item(std::string item) {
	for(int i = 0; i < (int)current_room->items.size(); i++) {
		if(current_room->items[i]->container && current_room->items[i]->state == OPEN) {
			for(int j = 0; j < (int)current_room->items[i]->content.size(); j++) {
				if(current_room->items[i]->content[j]->is_synonym(item)) {
					if(inventory_size + current_room->items[i]->content[j]->size <= INVENTORY_CAPACITY) {
						inventory.push_back(current_room->items[i]->content[j]);
						inventory_size += current_room->items[i]->content[j]->size;
						current_room->items[i]->content.erase(current_room->items[i]->content.begin() + j);
						return GET_OK;
					}
					return GET_FULL;
				}
			}
		}
		if(current_room->items[i]->is_synonym(item)) {
			if(!current_room->items[i]->fixed){
				if(inventory_size + current_room->items[i]->size <= INVENTORY_CAPACITY) {
					inventory.push_back(current_room->items[i]);
					inventory_size += current_room->items[i]->size;
					current_room->items.erase(current_room->items.begin() + i);
					return GET_OK;
				}
				return GET_FULL;
			}
			return GET_FIXED;
		}
	}
	return GET_FAIL;
}
//----------------------------------------------------------------------------
DropResult Player::drop_item(std::string item) {
	for(int i = 0; i < (int)inventory.size(); i++) {
		if(inventory[i]->is_synonym(item)) {
			current_room->items.push_back(inventory[i]);
			inventory_size -= inventory[i]->size;
			inventory.erase(inventory.begin() + i);
			return DROP_OK;
		}
	}
	return DROP_FAIL;
}
//----------------------------------------------------------------------------
EatResult Player::eat_item(std::string item) {
	for(int i = 0; i < (int)inventory.size(); i++) {
		if(inventory[i]->is_synonym(item)) {
			if(inventory[i]->edible) {
				health += inventory[i]->health_points;
				inventory_size -= inventory[i]->size;
				inventory.erase(inventory.begin() + i);
				return EAT_OK;
			}
			return EAT_CANT;
		}
	}
	return EAT_FAIL;
}
