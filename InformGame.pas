{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 12, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
program InteractiveGame;

{$mode objfpc}
{--------------------------------------------------------------------------------------------------}
uses
	Adventurer, Items, Parser, Locations, Shared;
{--------------------------------------------------------------------------------------------------}
type
	TDoor = class
	private
		DoorType: TUnlockingTypes;
		Status: (Unlocked, Locked);
		State: (Opened, Closed);
		Lead: TLocation;
	public
		constructor Create(UnlockedBy: TUnlockingTypes; LeadsTo: TLocation);
		function Unlock(Key: TKey): boolean;
		procedure Open;
		property Leads: TLocation read Lead;
	end;
{--------------------------------------------------------------------------------------------------}
constructor TDoor.Create(UnlockedBy: TUnlockingTypes; LeadsTo: TLocation);
begin
	Lead := LeadsTo;
	DoorType := UnlockedBy;
end;
{--------------------------------------------------------------------------------------------------}
function TDoor.Unlock(Key: TKey): boolean;
begin
	if DoorType = Key.KeyType then Status := Unlocked;
	Unlock := boolean(Status);
	WriteLn('You unlock the door with the key');
end;
{--------------------------------------------------------------------------------------------------}
procedure TDoor.Open;
begin
	case State of
		Opened:
			WriteLn('The door is already open');
		Closed:
		case Status of
			Unlocked:
			begin
				State := Opened;
				WriteLn('You open the door');
			end;
			Locked:
				WriteLn('The door appears to be locked');
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
var
	Player: TPlayer;
	Command: string;
	Map: array[0..1, 0..1] of TLocation;
{--------------------------------------------------------------------------------------------------}
procedure GenerateMap;
begin
	Map[0, 0] := TLocation.Create(RoomTitle, RoomDescription, Indoors, false);
	Map[0, 1] := TLocation.Create(RoomTitle, RoomDescription, Indoors, false);
	Map[1, 0] := TLocation.Create(RoomTitle, RoomDescription, Indoors, false);
	Map[1, 1] := TLocation.Create(RoomTitle, RoomDescription, Indoors, false);
	Map[0, 0].AddItem(TKey.Create(First));
	Map[1, 0].AddItem(TCandle.Create);
	Map[0, 0].AddPath(South, Map[1, 0]);
	Map[0, 1].AddPath(South, Map[1, 1]);
	Map[1, 0].AddPath(North, Map[0, 0]);
	Map[1, 0].AddPath(East, Map[1, 1]);
	Map[1, 1].AddPath(North, Map[0, 1]);
	Map[1, 1].AddPath(West, Map[1, 0]);
end;
{--------------------------------------------------------------------------------------------------}
begin
	GenerateMap;
	Player := TPlayer.Create(Map[0, 0]);
	Player.Location.Look;
	repeat
		Write('> ');
		ReadLn(Command);
		Parse(Player, Command);
	until false;
end.



{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 12, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description An interactive game similar to and inspired by the classical game Zork.              }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
program InteractiveGame;

{$mode objfpc}
{--------------------------------------------------------------------------------------------------}
uses
	Adventurer, Items, Parser, Portals, Locations, Shared;
{--------------------------------------------------------------------------------------------------}
type
	TRooms = (A, B, C, D);
{--------------------------------------------------------------------------------------------------}
var
	Player: TPlayer;
	Command: string;
	Map: array[TRooms] of TLocation;
{--------------------------------------------------------------------------------------------------}
procedure GenerateMap;
begin
	Map[A] := TLocation.Create('Room A', 'This is room A', Indoors, false);
	Map[B] := TLocation.Create('Room B', 'This is room B', Indoors, false);
	Map[C] := TLocation.Create('Room C', 'This is room C', Indoors, false);
	Map[D] := TLocation.Create('Room D', 'This is room D', Indoors, false);
	Map[A].AddItem(TKey.Create(First));
	Map[C].AddItem(TCandle.Create);
	Map[B].AddItem(TKey.Create(Second));
	Map[A].AddPath(South, Map[C]);
	Map[C].AddPath(East, Map[D]);
	Map[D].AddPath(North, Map[B], Door, First);
end;
{--------------------------------------------------------------------------------------------------}
begin
	GenerateMap;
	Player := TPlayer.Create(Map[A]);
	Player.Location.Brief;
	repeat
		WriteLn;
		Write('> ');
		ReadLn(Command);
		Parse(Player, Command);
	until false;
end.



{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 12, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
program InteractiveGame;

{$mode objfpc}
{--------------------------------------------------------------------------------------------------}
uses
	Classes, SysUtils;
{--------------------------------------------------------------------------------------------------}
type
	TUnlockingTypes = (First, Second, Third, Fourth, Fifth, Sixth);
	TDirections = (North, West, East, South);
	PString = array of string;

	TItem = class
		Title: string;
		Quantity: byte;
		//procedure PickUp;
		//procedure Drop;
	end;

	TMatches = class(TItem)
		constructor Create;
	end;

	TCandle = class(TItem)
		Lit: boolean;
		constructor Create;
		procedure Light(Match: TMatches);
	end;

	TKey = class(TItem)
		KeyType: TUnlockingTypes;
		constructor Create(Unlocks: TUnlockingTypes);
	end;

	PRoom = ^TRoom;

	TPassage = class
		Leads: PRoom;
		constructor Create(LeadsTo: PRoom);
	end;

	TDoor = class(TPassage)
		DoorType: TUnlockingTypes;
		Status: (Unlocked, Locked);
		State: (Opened, Closed);
		constructor Create(UnlockedBy: TUnlockingTypes; LeadsTo: PRoom);
		function Unlock(Key: TKey): boolean;
		procedure Open;
	end;

	TRoom = class
		Ways: array[TDirections] of TPassage; //Nil here is equal to a wall...
		Item: TItem;
		Dark: boolean;
		constructor Create(NorthPath, WestPath, EastPath, SouthPath, AnItem: TItem; IsDark: boolean);
		procedure Look;
	end;

	TPlayer = class
		Inventory: array of TItem;
		X, Y: byte;
		constructor Create;
		//procedure Drop;
	end;
{--------------------------------------------------------------------------------------------------}
constructor TMatches.Create;
begin
	Title := 'matches';
	Quantity := 4;
end;
{--------------------------------------------------------------------------------------------------}
constructor TCandle.Create;
begin
	Title := 'candle';
	Lit := false;
end;
{--------------------------------------------------------------------------------------------------}
procedure TCandle.Light(Match: TMatches);
begin
	if not Lit then begin
		Lit := true;
		WriteLn('You light the candle with the match');
	end else
		WriteLn('The candle is already lit');
end;
{--------------------------------------------------------------------------------------------------}
constructor TKey.Create(Unlocks: TUnlockingTypes);
begin
	Title := 'key';
	KeyType := Unlocks;
end;
{--------------------------------------------------------------------------------------------------}
constructor TPassage.Create(LeadsTo: PRoom);
begin
	Leads := LeadsTo;
end;
{--------------------------------------------------------------------------------------------------}
constructor TDoor.Create(UnlockedBy: TUnlockingTypes; LeadsTo: PRoom);
begin
	inherited Create(LeadsTo);
	DoorType := UnlockedBy;
end;
{--------------------------------------------------------------------------------------------------}
function TDoor.Unlock(Key: TKey): boolean;
begin
	if DoorType = Key.KeyType then Status := Unlocked;
	Unlock := boolean(Status);
	WriteLn('You unlock the door with the key');
end;
{--------------------------------------------------------------------------------------------------}
procedure TDoor.Open;
begin
	case State of
		Opened:
		WriteLn('The door is already open');
		Closed:
		case Status of
			Unlocked:
			begin
				State := Opened;
				WriteLn('You open the door');
			end;
			Locked:
			WriteLn('The door appears to be locked');
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
constructor TRoom.Create(NorthPath, WestPath, EastPath, SouthPath, AnItem: TItem; IsDark: boolean);
begin
	Ways[North] := NorthPath;
	Ways[West] := WestPath;
	Ways[East] := EastPath;
	Ways[South] := SouthPath;
	Doors := SomeDoors;
	Items := SomeItems;
	Dark := IsDark;
end;
{--------------------------------------------------------------------------------------------------}
procedure TRoom.Look;
begin
	if not Dark then begin
		//todo
	end else
		WriteLn('The room is pitch dark. You cannot see anything');
end;
{--------------------------------------------------------------------------------------------------}
constructor TPlayer.Create;
begin
	SetLength(Inventory, 10);
end;
{--------------------------------------------------------------------------------------------------}
var
	Player: TPlayer;
	Room: TRoom;
	Command: string;
{--------------------------------------------------------------------------------------------------}
function GetParts(KeyWord, Sentence: string): PString;
var
	Part: PString;
	Index: byte = 0;
	SplitAt: byte;
begin
	repeat
		SplitAt := Pos(KeyWord, Sentence);
		if SplitAt <> 0 then begin
			SetLength(Part, Index + 1);
			Part[Index] := Copy(Sentence, 1, SplitAt);
			Sentence := Copy(Sentence, SplitAt + 6, Length(Sentence));
			Inc(Index);
		end;
	until SplitAt = 0;
	Part[Index] := Sentence;
	GetParts := Part;
end;
{--------------------------------------------------------------------------------------------------}
procedure Interpret(Command: string);
var
	Part, Noun: Pstring;
	SplitAt: byte;
begin
	Trim(LowerCase(Command));

{---1---}
	Part := GetParts(' then ', Command);

{---2---}
	for Index := 0 to Length(Part) - 1 do begin
		//Kolla nycelord...
		SplitAt := Pos(' ', Part[Index]);
		Verb := Copy(Part[Index], 1, SplitAt);
		NounPhrase := Copy(Part[Index], SplitAt + 1, Length(Part[Index]));
	end;

{---A---}
	Noun := GetParts(' and ', NounPhrase);





	while Index <> Length(Command) do begin
		SetLength(Parts, Length(Parts) + 1);
		while Command[Index] <> #32 do begin
			Parts[Part] := Parts[Part] + Command[Index];
			Inc(Index);
		end;
		Inc(Part);
	end;

	case Parts[0] of
		'look':
		Room.Look;
		'get':
		case Parts[1] of
			'candle':
			if Room.Items.IndexOf(TCandle) <> -1 then begin
				Player.Inventory.Add(Room.Items.Extract(TCandle));
			end else
				WriteLn('There is no candle here');
			'matches':
			if Room.Items.IndexOf(TMatches) <> -1 then begin
				Player.Inventory.Add(Room.Items.Extract(TMatches));
			end else
				WriteLn('There are no matches here');
			'key':
			if Room.Items.IndexOf(TKey) <> -1 then begin
				Player.Inventory.Add(Room.Items.Extract(TKey));
			end else
				WriteLn('There is no key here');
			'':
			WriteLn('**Interpreter** You must specify a noun')
			else
			WriteLn('**Interpreter** I do not understand the word ''', Parts[1], '''');
		end;
		'unlock':
		case Parts[1] of

			'': WriteLn('')
			else
				WriteLn('');
		end;
		'light':
		case Parts[1] of
			'candle':;
			'torch':;
			'': WriteLn('')
			else
				WriteLn('');
		end;
		'': WriteLn('')
		else
			WriteLn('');
	end;
end;
{--------------------------------------------------------------------------------------------------}
begin
	repeat
		ReadLn(Command);
		Interpret(Command);
	until false;
end.