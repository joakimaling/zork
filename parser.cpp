#include "parser.h"
#include <algorithm>
#include <cstdlib>
//----------------------------------------------------------------------------
Parser::Parser() {
	parser_file.open("parsing/parser1.txt");
}
//----------------------------------------------------------------------------
Parser::~Parser() {
	parser_file.close();
}
//----------------------------------------------------------------------------
std::string Parser::get_verb() {
	return command.substr(0, command.find(' '));
}
//----------------------------------------------------------------------------
std::string Parser::get_noun() {
	return command.substr(command.find(' ') + 1, command.length() - 1);
}
//----------------------------------------------------------------------------
//std::string *Parser::split() {
//
//
//
//}
//----------------------------------------------------------------------------
std::string Parser::remove_articles() {



	return "";
}
//----------------------------------------------------------------------------
void Parser::parse_command(Player &player){

	std::string direction_full[] = {"north", "northeast", "east", "southeast", "south", "southwest", "west", "northwest", "up", "down", "in", "out"};

	std::cout << "> ";
	getline(std::cin, command);

	verb = get_verb(), noun = get_noun();

	if(command.compare("quit") == 0 || command.compare("exit") == 0) {
		exit(EXIT_SUCCESS);
	}
	else if(command.compare("inventory") == 0) {
		player.show_inventory();
		return;
	}
	else if(command.compare("look") == 0) {
		player.current_room->draw(true, player.lit);
		return;
	}

	for(int i = 0; i < 12; i++) {
		if(command.compare(direction_full[i]) == 0 || (noun.compare(direction_full[i]) == 0 && verb.compare("go") == 0)) {
			switch(player.go((Direction)i)) {
				case GO_FAIL:
					std::cout << "You can't go that way" << std::endl;
				return;
				case GO_OK:
					std::cout << "You go " << direction_full[i] << std::endl;
					player.current_room->draw(false, player.lit);
				return;
				case GO_CLOSED:
				return;
				case GO_WHERE:
					std::cout << "Go " << direction_full[i] << " where?" << std::endl;
				return;
			}
		}
	}

	if(verb.compare("examine") == 0) {
		if(!examine_item(player)) {
			examine_door(player);
		}
	}
	else if(verb.compare("open") == 0) {
		if(!open_item(player)) {
			open_door(player);
		}
	}
	else if(verb.compare("close") == 0) {
		if(!close_item(player)) {
			close_door(player);
		}
	}
	else if(verb.compare("get") == 0 || verb.compare("take") == 0) {
		switch(player.get_item(noun)) {
			case GET_OK:
				std::cout << "Taken." << std::endl;
			break;
			case GET_FAIL:
				std::cout << "You can't see any " << noun << " here!" << std::endl;
			break;
			case GET_FIXED:
				std::cout << "fixed" << std::endl;
			break;
			case GET_FULL:
				std::cout << "full" << std::endl;
			break;
		}
	}
	else if(verb.compare("eat") == 0) {
		switch(player.eat_item(noun)) {
			case EAT_OK:
				std::cout << "Eaten." << std::endl;
			break;
			case EAT_FAIL:
				std::cout << "You can't see any " << noun << " here!" << std::endl;
			break;
			case EAT_CANT:
				std::cout << "I don't think that the " << noun << " would agree with you." << std::endl;
			break;
		}
	}
	else if(verb.compare("drop") == 0) {
		switch(player.drop_item(noun)) {
			case DROP_OK:
				std::cout << "Dropped." << std::endl;
			break;
			case DROP_FAIL:
				std::cout << "Taken." << std::endl;
			break;
		}
	}
	else {
		std::cout << "I don't know the word \"" << verb << "\"" << std::endl;
	}
}
//----------------------------------------------------------------------------
bool Parser::examine_item(Player &player) {
	for(int i = 0; i < (int)player.current_room->items.size(); i++) {
		if(player.current_room->items[i]->is_synonym(noun)) {
			player.current_room->items[i]->examine();
			return true;
		}
	}
	return false;
}
//----------------------------------------------------------------------------
void Parser::examine_door(Player &player) {
	Door *tmp = player.current_room->get_door(noun);
	switch(tmp->examine()) {
		case CLOSED:
			std::cout << tmp->closed_statement << std::endl;
		break;
		case OPEN:
			std::cout << tmp->open_statement << std::endl;
		break;
	}
}
//----------------------------------------------------------------------------
bool Parser::open_item(Player &player) {
	for(int i = 0; i < (int)player.current_room->items.size(); i++) {
		if(player.current_room->items[i]->is_synonym(noun)) {
			switch(player.current_room->items[i]->open()) {
				case CONT_FAIL:
					std::cout << "You must tell me how to do that to a " << player.current_room->items[i]->name << "." << std::endl;
				break;
				case CONT_EMPTY:
					std::cout << "Opened." << std::endl;
				break;
				case CONT_ALREADY:
					std::cout << "It is already open." << std::endl;
				break;
			}
			return true;
		}
	}
	return false;
}
//----------------------------------------------------------------------------
void Parser::open_door(Player &player) {
	Door *tmp = player.current_room->get_door(noun);
	if(tmp->open()) {
		std::cout << tmp->opening_statement << std::endl;
	}
	else {
		std::cout << tmp->fail_statement << std::endl;
	}
}
//----------------------------------------------------------------------------
bool Parser::close_item(Player &player) {
	for(int i = 0; i < (int)player.current_room->items.size(); i++) {
		if(player.current_room->items[i]->is_synonym(noun)) {
			switch(player.current_room->items[i]->close()) {
				case CONT_FAIL:
					std::cout << "You must tell me how to do that to a " << player.current_room->items[i]->name << "." << std::endl;
				break;
				case CONT_OK:
					std::cout << "Closed." << std::endl;
				break;
				case CONT_ALREADY:
					std::cout << "It is already closed." << std::endl;
				break;
			}
			return true;
		}
	}
	return false;
}
//----------------------------------------------------------------------------
void Parser::close_door(Player &player) {
	Door *tmp = player.current_room->get_door(noun);
	if(tmp->close()) {
		std::cout << tmp->closing_statement << std::endl;
	}
	else {
		std::cout << tmp->fail_statement << std::endl;
	}
}
