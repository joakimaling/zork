#ifndef LOCATION_HPP
#define LOCATION_HPP

#include "item.hpp"
#include <string>
#include <vector>

namespace zork {
	class Location {
		public:
			Location(std::string name, std::string details): name(name), details(details) {}

		private:
			std::vector<Item> items;
			std::string name, details;
	};
}

#endif
