{Datum: 2009-05-31 Äventysspel med kommandotolk}
program Adventure;

const
	max_x = 10;
	max_y = 10;
	max_inv = 10;

type
	t_object = record
		name, description : string;
	end;
	t_room = record
		north, east, west, south : ^t_room;
		name, description : string;
		content : t_object;
		display : char; //Representation på skärmen?
	end;
	t_rooms = array[1..max_x,1..max_y] of t_room;

var
	room, current_room : t_rooms;
	inventory : array[1..max_inv] of t_object;

procedure initGame;
	var i : integer;
	begin
		for i := 1 to max_inv do inventory[i].name := 'Empty slot';
		for x := 1 to max_x do begin
			for y := 1 to max_y do begin
				//room[x,y].
			end;
		end;
	end;

procedure checkEnvironment;
	begin
		writeln(currnet_room.name);
		writeln(current_room.description);
	end;

function commandInterpreter(verb, noun : string) : char;
	begin
		if verb = 'look' then commandInterpreter := '0';
		if verb = 'go' then begin
			if noun = 'north' then commandInterpreter := 'n';
			if noun = 'south' then commandInterpreter := 's';
			if noun = 'east' then commandInterpreter := 'e';
			if noun = 'west' then commandInterpreter := 'w';
		end;
		if verb = 'get' then begin
			if noun = 'key' then commandInterpreter := 'k';
			if noun = 'map' then commandInterpreter := 'm';
			if noun = 'bottle' then commandInterpreter := 'b';
		end;
		if verb = 'open' then begin
			if noun = 'coffin' then commandInterpreter := 'c';
			if noun = 'door' then commandInterpreter := 'd';
		end
		if verb = 'examine' then;
		end
		else begin
			writeln('I do not understand the words ', verb, ' and ', noun);
			commandInterpreter := 'X';
		end;
	end;

begin
	initGame;
	repeat
		checkEnvironment;
		repeat
			readln(verb, noun);
			command := commandInterpreter(verb, noun);
		until command <> 'X';

	until quit;
end.



{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 14, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit Adventurer;

{$mode objfpc}

interface
{--------------------------------------------------------------------------------------------------}
uses
	Items, Locations, Shared;
{--------------------------------------------------------------------------------------------------}
type
	TPlayer = class
	private
		Inventory: array of TItem;
		LastLocation, CurrentLocation: TLocation;
		Health: byte;
		function SeekInventory(Item: string): TItem;
		function InventoryFree: byte;
	public
		constructor Create(Place: TLocation);
		procedure View; //Reveals the contents of the inventory...
		procedure Diagnose;
		procedure Get(Item: string);
		procedure Go(Direction: TDirections);
		procedure Drop(Item: string);
		procedure Eat(Item: string);
		property Location: TLocation read CurrentLocation;
	end;
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
constructor TPlayer.Create(Place: TLocation);
var
	Index: byte;
begin
	SetLength(Inventory, 22);
	for Index := 0 to Length(Inventory) - 1 do Inventory[Index] := None;
	CurrentLocation := Place;
	LastLocation := CurrentLocation;
	Health := 100;
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.View;
var
	Index: byte;
begin
	WriteLn('You have:');
	for Index := 0 to Length(Inventory) - 1 do
		if Inventory[Index] <> nil then WriteLn('  * ', Inventory[Index].Name);
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Diagnose;
begin
	case Health of
		100:
			WriteLn('You are in splendid health');
		50..99:
			WriteLn('You are wounded lightly. You will be cured in ', 100 - Health, ' moves');
		1..49:
			WriteLn('You are seriously wounded! You will be cured in ', 100 - Health, ' moves');
	end;
end;
{--------------------------------------------------------------------------------------------------}
function TPlayer.SeekInventory(Item: string): TItem; //Returns the item which carries the given name. If not
var                                          //found in inventory nil is returned...
	Index: byte;
begin
	SeekInventory := None;
	for Index := 0 to Length(Inventory) - 1 do
		if (Inventory[Index] <> None) and (Inventory[Index].Name = Item) then begin
			SeekInventory := Inventory[Index];
			break;
		end;
end;
{--------------------------------------------------------------------------------------------------}
function TPlayer.InventoryFree: byte; //Returns the first index + 1 where a free slot in the inventory is
var                           //if no free space is there 0 is returned...
	Index: byte;
begin
	InventoryFree := 0;
	for Index := 0 to Length(Inventory) - 1 do
		if Inventory[Index] = None then begin
			InventoryFree := Index + 1;
			break;
		end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Get(Item: string);
var
	GottenItem: TItem;
	Index: byte;
begin
	Index := InventoryFree;
	if Index <> 0 then begin
		GottenItem := CurrentLocation.GetItem(Item);
		if GottenItem <> None then begin
			Inventory[Index - 1] := GottenItem;
			WriteLn('Taken');
		end else
			WriteLn('There is no ', Item, ' here');
	end else
		WriteLn('Your inventory is full');
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Go(Direction: TDirections);
begin
	if CurrentLocation.Path[Direction] <> Blocked then begin
		if Direction = Back then
			CurrentLocation := LastLocation
		else if CurrentLocation.Port[Direction] = None then begin
			LastLocation := CurrentLocation;
			CurrentLocation := TLocation(CurrentLocation.Path[Direction]);
		end else
			case CurrentLocation.Port[Direction].State of
				Ajar, Closed:
					WriteLn('You should open the ', CurrentLocation.Port[Direction].Name, ' before you can go through it');
				Opened:
				begin
					LastLocation := CurrentLocation;
					CurrentLocation := TLocation(CurrentLocation.Path[Direction]);
				end;
			end;
		CurrentLocation.Brief;
	end else
		WriteLn('You cannot go there');
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Drop(Item: string);
var
	Index: byte;
begin
	for Index := 0 to Length(Inventory) - 1 do
		if (Inventory[Index] <> None) and (Inventory[Index].Name = Item) then begin
			CurrentLocation.AddItem(Inventory[Index]);
			Inventory[Index] := None;
			WriteLn('Dropped');
			exit;
		end;
	WriteLn('You do not have a ', Item);
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Eat(Item: string);
var
	GottenItem: TItem;
	Found: boolean = false;
begin
	GottenItem := SeekInventory(Item);
	if GottenItem <> None then
		Found := true
	else begin
		GottenItem := CurrentLocation.GetItem(Item);
		if GottenItem <> None then
			Found := true
		else
			WriteLn('There is no ', Item, ' here');
	end;
	if Found then
		if GottenItem.Food <> 0 then begin
			Inc(Health, Ord(Health + GottenItem.Food <= 100) * GottenItem.Food);
			GottenItem.Free; //The item's eaten and thus doesn't exist anymore...
			WriteLn('Eaten');
		end	else
			WriteLn('You cannot eat a ', GottenItem.Name);
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.



{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... September 14, 2011                                                                   }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit Adventurer;

{$mode objfpc}

interface
{--------------------------------------------------------------------------------------------------}
uses
	Items, Locations, Shared;
{--------------------------------------------------------------------------------------------------}
type
	TPlayer = class
	private
		Inventory: array of TItem;
		LastLocation, CurrentLocation: TLocation;
		Health: byte;
	public
		constructor Create(Place: TLocation);
		procedure View; //Reveals the contents of the inventory...
		procedure Diagnose;
		procedure Go(Direction: TDirections);
		procedure Drop(Item: string);
		procedure Eat(Item: TItem);
		property Location: TLocation read CurrentLocation;
	end;
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
constructor TPlayer.Create(Place: TLocation);
var
	Index: byte;
begin
	SetLength(Inventory, 22);
	for Index := 0 to Length(Inventory) - 1 do Inventory[Index] := None;
	CurrentLocation := Place;
	LastLocation := CurrentLocation;
	Health := 100;
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.View;
var
	Index: byte;
begin
	WriteLn('You have:');
	for Index := 0 to Length(Inventory) - 1 do
		if Inventory[Index] <> nil then WriteLn('  * ', Inventory[Index].Name);
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Diagnose;
begin
	case Health of
		100:
			WriteLn('You are in splendid health');
		99..50:
			WriteLn('You are wounded lightly. You will be cured in ', 100 - Health, ' moves');
		49..1:
			WriteLn('You are seriously wounded! You will be cured in ', 100 - Health, ' moves');
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Go(Direction: TDirections);
begin
	case Direction of
		Back:
		begin
			CurrentLocation := LastLocation;
			WriteLn('You go back');
		end;
		East:
		if CurrentLocation.GetPath(East) <> Wall then begin
			LastLocation := CurrentLocation;
			CurrentLocation := TLocation(CurrentLocation.GetPath(East));
			WriteLn('You go east');
		end else
			WriteLn('You cannot go there');
		North:
		if CurrentLocation.GetPath(North) <> Wall then begin
			LastLocation := CurrentLocation;
			CurrentLocation := TLocation(CurrentLocation.GetPath(North));
			WriteLn('You go north');
		end else
			WriteLn('You cannot go there');
		South:
		if CurrentLocation.GetPath(South) <> Wall then begin
			LastLocation := CurrentLocation;
			CurrentLocation := TLocation(CurrentLocation.GetPath(South));
			WriteLn('You go south');
		end else
			WriteLn('You cannot go there');
		West:
		if CurrentLocation.GetPath(West) <> Wall then begin
			LastLocation := CurrentLocation;
			CurrentLocation := TLocation(CurrentLocation.GetPath(West));
			WriteLn('You go west');
		end else
			WriteLn('You cannot go there');
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Drop(Item: string);
var
	Index: byte;
begin
	for Index := 0 to Length(Inventory) - 1 do
		if Inventory[Index].Name = Item then begin
			CurrentLocation.AddItem(Inventory[Index]);
			Inventory[Index] := None;
			WriteLn('You dropped a ', Item);
		end;
	WriteLn('You do not have a ', Item);
end;
{--------------------------------------------------------------------------------------------------}
procedure TPlayer.Eat(Item: TItem);
begin
	if Item.Food <> 0 then begin
		Inc(Health, Ord(Health + Item.Food <= 100) * Item.Food); //Unless it's too much; heal...
		Item.Free; //The item's eaten and thus doesn't exist anymore...
	end	else
		WriteLn('You cannot eat a ', Item.Name);
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.