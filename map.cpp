#include "map.h"
#include <fstream>
#include <algorithm>
#include <cstdlib>
//----------------------------------------------------------------------------
Map::Map() {
	for(int i = 0; i < MAP_SIZE; i++) {
		map_grid[i] = NULL;
	}
}
//----------------------------------------------------------------------------
Map::~Map() {
	for(int i = 0; i < MAP_SIZE; i++) {
		if(map_grid[i]) {
			delete map_grid[i];
		}
	}
}
//----------------------------------------------------------------------------
void Map::load_from_file(const char *file) {
	std::ifstream infile(file);
	if(infile.is_open()) {
		while(!infile.eof()) {
			infile >> title;
			if(std::string::npos != title.find("~")) {
				break;
			}
			infile >> id >> inside >> dark >> desc;
			map_grid[--id] = new Room;
			map_grid[id]->name = edit_string(title);
			map_grid[id]->inside = inside;
			map_grid[id]->description = edit_string(desc);
			map_grid[id]->dark = dark;
		}
		while(!infile.eof()) {
			infile >> title;
			if(std::string::npos != title.find("~")) {
				break;
			}
			infile >> id >> opening >> closing >> fail >> closed >> open;
			door[--id] = new Door;

			infile >> next;
			if(std::string::npos != next.find("(")) {
				while(true) {
					infile >> next;
					if(std::string::npos != next.find(")")) {
						break;
					}
					door[id]->synonyms.push_back(next);
				}
			}

			door[id]->name = edit_string(title);
			door[id]->opening_statement = edit_string(opening);
			door[id]->closing_statement = edit_string(closing);
			door[id]->fail_statement = edit_string(fail);
			door[id]->open_statement = edit_string(open);
			door[id]->closed_statement = edit_string(closed);
		}
		while(!infile.eof()) {
			infile >> title;
			if(std::string::npos != title.find("~")) {
				break;
			}
			infile >> id >> item_id >> room_id >> size >> container >> fixed >> inflammable >> edible >> transparent >> state >> visible >> health >> desc;
			item[--id] = new Item;
			if(item_id == 0) {
				map_grid[room_id - 1]->items.push_back(item[id]);
			}
			else {
				item[item_id - 1]->content.push_back(item[id]);
			}

			infile >> next;
			if(std::string::npos != next.find("(")) {
				while(true) {
					infile >> next;
					if(std::string::npos != next.find(")")) {
						break;
					}
					item[id]->synonyms.push_back(next);
				}
			}

			item[id]->name = edit_string(title);
			item[id]->size = size;
			item[id]->container = (bool)container;
			item[id]->fixed = (bool)fixed;
			item[id]->inflammable = (bool)inflammable;
			item[id]->edible = (bool)edible;
			item[id]->transparent = (bool)transparent;
			item[id]->state = (DoorState)state;
			replace(desc.begin(), desc.end(), '#', '\n');
			item[id]->description = edit_string(desc);
			item[id]->visible = (bool)visible;
			item[id]->health_points = health;
		}
		while(!infile.eof()) {
			infile >> id;
			for(int i = 0; i < DIR_SIZE - 2; i++) {
				infile >> door_id >> room_id;
				map_grid[id - 1]->path[i].room = (room_id == 0) ? NULL : map_grid[--room_id];
				map_grid[id - 1]->path[i].door = (door_id == 0) ? NULL : door[--door_id];
			}
		}
	}
}
//----------------------------------------------------------------------------
std::string Map::edit_string(std::string str) {
	replace(str.begin(), str.end(), '_', ' ');
	return str;
}
