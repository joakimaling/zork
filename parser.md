COMMAND         → ACTION_PHRASE
                | NOUN_PHRASE, ACTION_PHRASE

ACTION_PHRASE   → VERB_PHRASE THEN_WORD ACTION_PHRASE
                |	VERB_PHRASE

NOUN_PHRASE     → BASIC_NP CONNECTIVE NOUN_PHRASE
                | BASIC_NP

VERB_PHRASE     → IMPERATIVE_VERB GRAMMAR_LINE
                | again

THEN_WORD       → then, (.)

BASIC_NP        → DESCRIPTOR_LIST NOUN_LIST

CONNECTIVE      → AND_WORD
                | BUT_WORD

IMPERATIVE_VERB → answer, attack, blow, board, break, burn, climb, close, count,
                  cross, cut, deflate, dig, disembark, drink, drop, eat, enter,
                  examine, exit, extinguish, fill, follow, get, give, go,
                  inflate, jump, kick, kill, knock, launch, light, listen, lock,
                  look, lower, move, open, pour, pray, pull, push, put, raise,
                  read, say, search, shake, slide, smell, stab, stay, strike,
                  swim, take, tell, throw, tie, touch, turn, unlock, wake, walk,
                  wave, wear, wind

GRAMMAR_LINE    → NOUN_PHRASE GRAMMAR_LINE
                | NUMBER GRAMMAR_LINE
                | PREPOSITION GRAMMAR_LINE
                | UNPARSED_TEXT GRAMMAR_LINE

DESCRIPTOR_LIST → DESCRIPTOR DESCRIPTOR_LIST
                | DESCRIPTOR

NOUN_LIST       → NOUN NOUN_LIST
                | NOUN

AND_WORD        → also, and, (,)

BUT_WORD        → but, except

PREPOSITION     → at, beneath, beside, in, inside, into, on, to, under,
                  underneath

DESCRIPTOR      → ALL_WORD
                | ARTICLE
                | DEMANDING_NUMBER
                | DEMOSTRATIVE_ADJECTIVE
                | my
                | other

NOUN            → NAME
                | PRONOUN
                | me

ALL_WORD        →	all, every

ARTICLE         → a, an, some, the

DEMOSTRATIVE_ADJECTIVE → that, these, this, those

NAME            → back, buoy, candle, door, down, east, gnome, key, label, lamp,
                  leaflet, mailbox, north, northeast, northwest, painting, rope,
                  sword, south, southeast, southwest, troll, up, west

PRONOUN         → her, him, it, them
