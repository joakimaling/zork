#include "zork/parser.hpp"

int main() {
	zork::Parser parser;

	std::cout << "ZORK I: The Great Underground Empire\n";
	std::cout << "Copyright (c) 1981, 1982, 1983 Infocom, Inc. All rights reserved.\n";
	std::cout << "ZORK is a registered trademark of Infocom, Inc.\n";
	std::cout << "Revision 88 / Serial number 840726\n\n";

	while (true) {
		parser.run();
	}

	return 0;
}
